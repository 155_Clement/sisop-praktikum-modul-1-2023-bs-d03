#!bin/bash

sen=0
user=0
write_log() {
        if [ $sen -lt 1 ]; then
                echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $user" >> log.txt
        else
                echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: INFO User $user logged in" >> log.txt
        fi
}
#---------------------------------------
echo "Login..."
store="$(dirname "$0")/users/users.txt"

sen=0
while [ $sen -lt 1 ]
do
        read -p "Input username: " user
	r_pass="$( awk -v user="$user" -F',' '{if ($1==user) {print $2}}' $store )"
	read -p "Input password: " pass
	if [[ $pass == $r_pass ]]; then
                sen=1
        	write_log
		echo "Login success!"
	else
                echo "Wrong Password!"
                write_log
        fi
done
