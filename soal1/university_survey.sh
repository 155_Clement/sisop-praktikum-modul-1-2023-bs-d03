#! /bin/bash
#Change decimal separator from comma (ID) to dot (US)
export LC_NUMERIC="en_US.UTF-8"

#No A
head -n 1 input.csv | tr ',' '|'
echo "Top 5 Universities in Japan"
awk -F',' 'NR>2 {if ($3=="JP") {print $0}}' input.csv | tr ',' ' ' | head -n 5
#for every line after second line, if third column = "JP", pipe result, remove comma, print only 5 top result.
IFS=$'\n' japan=($(awk -F',' 'NR>2 {if ($3=="JP") {print $0}}' input.csv))
#declare delimiter as newline, store output in array

#No B
echo $'\n'"5 Universities in Japan with lowest Faculty Student Score (Name, score)"
mapfile -t japan_sorted < <(printf "%s\n" "${japan[@]}" | sort -t, -k9,9 -n)
#Untuk setiap baris, menggunakan delimiter ',', sort menggunakan kolom ke 9,9 lalu simpan ke mapfile
IFS=','

for ((x=0; x<5; x++))
do
	read -a strarr <<< "${japan_sorted[$x]}"
	echo "${strarr[1]}|${strarr[8]}"
done
#Menggunakan delimiter ',', baca string pada array ke strarr, lalu baca nama univ, skor fsr

#No C
echo $'\n'"10 Universities in Japan with highest Employment Outcome Rank (Name, ranking)"
mapfile -t japan_sorted < <(printf "%s\n" "${japan[@]}" | sort -t, -k20,20 -n)
#Untuk setiap baris, menggunakan delimiter ',', sort menggunakan kolom ke 20,20 lalu simpan ke mapfile
IFS=','
for ((x=0; x<10; x++))
do
	read -a strarr <<< "${japan_sorted[$x]}"
	echo "${strarr[1]}|${strarr[19]}"
done
#Menggunakan delimiter ',', baca string pada array ke strarr, lalu baca nama univ, ranking ger

#No D
echo $'\n''Search result using query = "keren"'

while IFS=',' read -a line
do
	if [[ ${line[1],,} == *"keren"* ]]; then
		echo "${line[1]}"
	fi
done < input.csv
