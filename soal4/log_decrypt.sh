#! /bin/bash

abs_path=./$1
#For getting hour
a=$1
curHour=${a:0:2}
curHour=$((10#$curHour))
curHour=$((26-$curHour))

#USED FOR ENCRYPTION AND DECRYPTION
az=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
az_cap=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
while IFS= read -r line; do
	echo $line | tr "${az:0:26}${az_cap:0:26}" "${az:$curHour:26}${az_cap:$curHour:26}" >> "decrypted_$1.txt"
done < "$abs_path"
