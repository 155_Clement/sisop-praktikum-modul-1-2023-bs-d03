#!/bin/bash

# cronjob command : 
# * */10 * * * bash /home/navahindra/sisop-praktikum-modul-1-2023-bs-d03/soal2/kobeni_liburan.sh

#take the current hour as the amount of images downloaded
hour=$(date +%H)
amount=$hour
if [ $amount -eq 0 ]
then amount=1
fi

folder_count=$(find . -maxdepth 1 -type d -name "kumpulan_*" | wc -l)
zip_count=$(ls devil_*.zip 2>/dev/null | wc -l)

if [ $zip_count == 0 ]
then creation_day=00
else
    creation_day=$(date -d $(stat -c %y "devil_$zip_count.zip" | cut -d' ' -f1) +%d)
fi

current_date=$(date +%d)

folder_count=$((folder_count + 1))
mkdir kumpulan_$folder_count

for ((i=1;i<=$amount ;i++))
    do
        wget -O kumpulan_$folder_count/perjalanan_$i https://source.unsplash.com/random/800x600/?indonesia
    done

if [ $current_date != $creation_day ]
then 
    echo "Zipping..."
    zip_count=$((zip_count + 1))
    for ((i=1;i<=$folder_count;i++))
        do
            zip -r devil_$zip_count.zip kumpulan_$i
            rm -rf kumpulan_$i
        done
fi
