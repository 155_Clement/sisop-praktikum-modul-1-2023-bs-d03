# sisop-praktikum-modul-1-2023-BS-D03

## Nomor 1
**Dalam pengerjaan, file "2023 QS World University Rankings.csv" diubah menjadi "input.csv"**

###### A. Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

```
#Change decimal separator from comma (ID) to dot (US)
export LC_NUMERIC="en_US.UTF-8"
```
Karena dalam file .csv, nilai desimal ditulis menggunakan '.', maka harus diganti setting dari sistem ID menjadi AS.

Karena file .csv sudah diurutkan berdasarkan ranking, maka hanya perlu mencari 5 universitas di Jepang yang pertama kali ditemukan jika dicari dari atas ke bawah.
```
head -n 1 input.csv | tr ',' '|'
```
Pertama, menampilkan header file .csv dengan mengganti tanda ',' dengan '|'
```
awk -F',' 'NR>2 {if ($3=="JP") {print $0}}' input.csv | tr ',' ' ' | head -n 5
```
- `awk -F','`, menggunakan awk dengan delimiter ',' untuk membaca setiap kolom sebagai variabel terpisah.
- `'NR>2 {if ($3=="JP") {print $0}}' input.csv`, membaca file input.csv, dimulai dari baris kedua (setelah header) mencetak sebagai output setiap baris dimana kolom ketiga (*location code*) memiliki nilai "JP" (lokasi di Jepang).
- `tr ',' ' ' | head -n 5`, hasil output awk dipipe ke tr dimana tanda koma diganti dengan spasi agar lebih mudah dibaca lalu ditampilkan 5 hasil pertama.


###### Digunakan untuk no 2 dan 3
```
IFS=$'\n' japan=($(awk -F',' 'NR>2 {if ($3=="JP") {print $0}}' input.csv))
```
Berfungsi untuk mencari semua universitas yang berada di Jepang, lalu menyimpannya di dalam array bernama `japan`. 
- `IFS=$'\n'` berarti tanda '\n' digunakan sebagai pemisah antar anggota sehingga setiap baris disimpan dalam index sendiri-sendiri.

###### B. Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 

```
mapfile -t japan_sorted < <(printf "%s\n" "${japan[@]}" | sort -t, -k9,9 -n)
```
- `printf "%s\n" "${japan[@]}" | sort -t, -k9,9 -n`, seluruh isi array `japan` akan dicetak menggunakan printf lalu dipipe ke sort yang akan menyortir berdasarkan : `-t,`(Menganggap ',' sebagai pemisah antar kolom) `-k9,9`(Menyortir menggunakan kolom ke-9 saja) `-n`(Mode menyortir angka).
- `mapfile -t japan_sorted < <(printf "%s\n" "${japan[@]}" | sort -t, -k9,9 -n)`, hasil sorting kemudian dipindahkan (menggunakan proccess subtitution "<()" karena mapfile tidak bisa menggunakan redirect saja) ke dalam mapfile `japan_sorted` (-t berarti newline setiap baris tidak disimpan).

```
IFS=','

for ((x=0; x<5; x++))
do
	read -a strarr <<< "${japan_sorted[$x]}"
	echo "${strarr[1]}|${strarr[8]}"
done
```
- `read -a strarr <<< "${japan_sorted[$x]}"`, untuk setiap baris yang tersimpan dalam `japan_sorted`, dipass (<<<) sebagai string dan dibaca ke dalam array `strarr`. `IFS=','` berarti ',' dianggap sebagai pemisah antar kolom.
- `echo "${strarr[1]}|${strarr[8]}"`, mencetak hanya kolom 2 dan 9 (nama universitas dan frs score).

###### C. Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
```
echo $'\n'"10 Universities in Japan with highest Employment Outcome Rank (Name, ranking)"
mapfile -t japan_sorted < <(printf "%s\n" "${japan[@]}" | sort -t, -k20,20 -n)
#Untuk setiap baris, menggunakan delimiter ',', sort menggunakan kolom ke 20,20 lalu simpan ke mapfile
IFS=','

for ((x=0; x<10; x++))
do
	read -a strarr <<< "${japan_sorted[$x]}"
	echo "${strarr[1]}|${strarr[19]}"
done
#Menggunakan delimiter ',', baca string pada array ke strarr, lalu baca nama univ, ranking ger
```
Kode mirip dengan poin B, tetapi ada sedikit perbedaan :
- `printf "%s\n" "${japan[@]}" | sort -t, -k20,20 -n`, sorting berdasarkan kolom ke-20 (ranking ger).
- `for ((x=0; x<10; x++))`, mengambil 10 universitas.
- `echo "${strarr[1]}|${strarr[19]}"`, mencetak nama dan rangking ger.

###### D. Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci "keren".
```
echo $'\n''Search result using query = "keren"'

while IFS=',' read -a line
do
	if [[ ${line[1],,} == *"keren"* ]]; then
		echo "${line[1]}"
	fi
done < input.csv
```
- `while IFS=',' read -a line`, while loop berjalan selama masih terdapat line yang bisa dibaca dengan ',' digunakan sebagai pemisah antar kolom.
- `if [[ ${line[1],,} == *"keren"* ]]`, setiap baris dari file .csv dibaca sebagai string dengan semua karakter diubah menjadi lowercase (,,). Apabila terdapat pola "keren", maka cetak.

## Nomor 2 
Script diharuskan untuk mendownload gambar tentang Indonesia sebanyak X, dimana X adalah jam saat ini. Download dilakukan setiap 10 jam sekali, maka perlu mengambil waktu saat ini sebagai variabel jumlah gambar yang di download. 
```
hour=$(date +%H)
amount=$hour
if [ $amount -eq 0 ]
then amount=1
fi
``` 
Kode diatas mengambil jam saat ini sebagai variabel hour, kemudian dioper ke variabel amount, jika saat ini pukul 00:00 maka download 1 gambar.  
Selanjutnya gambar yang di download disimpan dengan format "perjalanan_Nomor.file" di dalam suatu folder bernama "kumpulan_Nomor.folder", maka dari itu saat program dijalankan akan mendeteksi folder yang ada di folder saat ini agar urutan sequences folder tetap terurut.
```
folder_count=$(find . -maxdepth 1 -type d -name "kumpulan_*" | wc -l)
zip_count=$(ls devil_*.zip 2>/dev/null | wc -l)
```
Kode diatas menghitung jumlah folder yang ada di current directory dengan format nama kumpulan_\* dan juga menghitung jumlah zip file dengan format nama devil_\*.
```
if [ $zip_count == 0 ]
then creation_day=00
else
    creation_day=$(date -d $(stat -c %y "devil_$zip_count.zip" | cut -d' ' -f1) +%d)
fi

current_date=$(date +%d)
```
Jika tidak ada file zip maka creation_day = 0, jika ada maka creation_day akan mengambil tanggal berapa zip itu dibuat. hal ini digunakan untuk melakukan zip setiap hari tanpa cronjob karena cronjob digunakan dengan tugas menjalankan program setiap 10 jam sekali.
```
folder_count=$((folder_count + 1))
mkdir kumpulan_$folder_count

for ((i=1;i<=$amount ;i++))
    do
        wget -O kumpulan_$folder_count/perjalanan_$i https://source.unsplash.com/random/800x600/?indonesia
    done
```
folder count ditambahkan 1, karena kita ingin membuat sequences folder selanjutnya lalu download gambar sebanyak `$amount` yang sesuai dengan jam saat ini. Kemudian hasil download disimpan kedalam folder yang telah dibuat.  
  
Disini kami mengalami kendala untuk memikirkan cara agar file zip dapat dibuat setiap sehari sekali, karena cronjob sudah digunakan untuk menjalankan program ini dengan waktu setiap 10 jam setelah program berjalan, maka ini adalah pendekatan yang kami gunakan.
```
if [ $current_date != $creation_day ]
then 
    echo "Zipping..."
    zip_count=$((zip_count + 1))
    for ((i=1;i<=$folder_count;i++))
        do
            zip -r devil_$zip_count.zip kumpulan_$i
            rm -rf kumpulan_$i
        done
fi
```
`creation_day` adalah tanggal zip terakhir yang dibuat sedangkan `current_date` mengambil tanggal saat ini. Jika tanggalnya sudah berbeda maka proses zip akan dijalankan. Semua folder yang memiliki format kumpulan_\* akan dimasukkan ke dalam zip kemudian dihapus dari current folder. Setelah itu file zip disimpan di current folder dengan format devil_\*. Sequence folder kumpulan akan kembali menghitung dari angka 1.

Cronjob yang digunakan untuk menjalankan program ini setiap 10 jam sekali adalah  
```
* */10 * * * bash /home/navahindra/sisop-praktikum-modul-1-2023-bs-d03/soal2/kobeni_liburan.sh
```
`navahindra` dapat diganti dengan nama user saat ini.

## Nomor 3

###### Fungsi "write_log()"

Karena untuk script "louis.sh" dan "retep.sh" harus mencetak pesar error ke dalam "log.txt", maka digunakan 1 fungsi yang bernama "write_log".

```
sen=0
write_log() {
	if [ $sen -lt 1 ]; then
		echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
	else
		echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $user registered successfully" >> log.txt
	fi
}
```
Fungsi menggunakan variabel "sen" untuk mengetahui error yang harus dicetak. Pada kedua script, "0" berarti ada masalah dan "1" berarti lancar. Fungsi juga mencetak tanggal menggunakan panggilan "date".

###### louis.sh
```
user=0
#Fungsi write_log()....

echo "Registering new user..."
store="$(dirname "$0")/users/users.txt"
```
`$(dirname "$0")` akan mengembalikan folder lokasi dijalankannya script. "store" digunakan agar script bisa mengetahui lokasi file "users.txt".

```
sen=0
while [ $sen -lt 1 ]
do
	read -p "Input username: " user
	if ! grep -qw -m 1 $user "$store"; then
		sen=1
	else
		echo "Username already exists!"
		write_log
	fi
done
```
- Loop akan dilakukan selama user memasukkan username yang sudah terdaftar. "write_log" dipanggil untuk mencetak error.
- `! grep -qw -m 1 $user "$store";` , grep mencari apakah username terdapat dalam "users.txt". Grep hanya mencari kata utuh (w) dan return setelah menemukan 1 pasangan "-m 1" tanpa mengeluarkan output ke terminal (q).
Maka jika tidak ditemukan match (return 0) username dianggap valid.


Untuk selanjutnya, program masuk pada while loop yang serupa namun dengan tujuan memastikan bahwa password valid. Berikut adalah penjelasan masing-masing kondisi.

- Minimal 8 karakter
```
[[ ${#pass} -ge 8 ]]
```
Script membaca panjang string password (${#pass}) dan membandingkannya.

- Minimal 1 karakter uppercase dan 1 lowercase
```
[[ $pass =~ [A-Z] ]] && [[ $pass =~ [a-z] ]]
```
Script mencari apakah ada karakter dalam set didalam string. Kondisi dipenuhi jika ditemukan karakter A-Z (uppercase) dan a-z (lowercase).

- Alphanumeric
```
[[ $pass =~ [^A-Za-z0-9] ]]
```
Kondisi dipenuhi apabila tidak terdapat karakter SELAIN A-Z, a-z, dan 0-9.

- Tidak bisa sama dengan username + Tidak boleh ada substring "chicken" atau "ernie"
```
 [[ $pass == $user ]] || ([[ "${pass,,}" =~ chicken ]] || [[ "${pass,,}" =~ ernie ]])
```
Kondisi dipenuhi jika terdeteksi bahwa password sama dengan username ATAU ditemukan salah satu kata terlarang. Dalam perbandingan, password diubah menjadi lowercase ("${pass,,}").

- Jika password valid
```
echo "Success"
sen=1
write_log
```
Maka program berhenti looping dan dicetak log INFO.


```
echo $user,$pass >> $store

```
Username dan password valid dicetak ke dalam users.txt dengan pemisah koma ",".

###### retep.sh
```
sen=0
user=0
write_log() {
        if [ $sen -lt 1 ]; then
                echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $user" >> log.txt
        else
                echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: INFO User $user logged in" >> log.txt
        fi
}
#---------------------------------------
echo "Login..."
store="$(dirname "$0")/users/users.txt"
```
Sama dengan "louis.sh" kecuali pada pesan yang dicetak.

Selanjutnya, program masuk pada while loop untuk memeriksa password.
```
sen=0
while [ $sen -lt 1 ]
do
        read -p "Input username: " user
	r_pass="$( awk -v user="$user" -F',' '{if ($1==user) {print $2}}' $store )"
	read -p "Input password: " pass
	if [[ $pass == $r_pass ]]; then
                sen=1
        	write_log
		echo "Login success!"
	else
                echo "Wrong Password!"
                write_log
        fi
done
```
Setelah menerima username, program akan mengambil password username tersebut dari file "users.txt".
```
r_pass="$( awk -v user="$user" -F',' '{if ($1==user) {print $2}}' $store )"
```
Menggunakan delimiter "," dicari username lalu disimpan password username, yaitu string di sebelah kanan username (dipisah dengan koma).

```
	read -p "Input password: " pass
	if [[ $pass == $r_pass ]]; then
                sen=1
        	write_log
		echo "Login success!"
	else
                echo "Wrong Password!"
                write_log
```
Setelah input untuk password diterima,  akan dibandingkan dengan password pada "users.txt". Pesan akan dicetak tergantung dari hasil perbandingan. Jika gagal, maka pesan error disimpan dan looping dari awal (menerima username). Jika tidak pesan sukses disimpan dan program keluar.


## Nomor 4


###### log_encrypt.sh
```
curTime=$(date +"%H:%M %d:%m:%Y")
curHour=$(date +"%H")
curHour=$((10#$curHour))

syslog_path="/var/log/syslog"
```
Script membaca waktu dan tanggal untuk digunakan sebagai nama file backup (.txt) dan sebagai key caesar cipher yang digunakan.
- `curHour=$((10#$curHour))`, karena jam 1-9 ditulis sebagai 01-09, maka harus dihapus '0' di depannya.
- Untuk mencari file "syslog", digunakan absolute path.
```
az=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
az_cap=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
```
Set karakter yang digunakan untuk enkripsi dan dekripsi.
```
while IFS= read -r line; do
	echo $line | tr "${az:0:26}${az_cap:0:26}" "${az:$curHour:26}${az_cap:$curHour:26}" >> "${curTime}.txt"
done < "$syslog_path"
```
- `while IFS= read -r line`, program membaca setiap baris.
- `echo $line | tr "${az:0:26}${az_cap:0:26}" "${az:$curHour:26}${az_cap:$curHour:26}"`, program membaca baris lalu dipipe ke command `tr`. Menggunakan set karakter yang sudah dideklarasi, setiap karakter digeser maju sebanyak jumlah yang diatur sebelumnya (berdasarkan jam backup). Untuk melakukannya, digunakan "parameter expansion" yaitu ${string:offset:panjang string}.
- `"$syslog_path"`, variabel yang menyimpan absolute path file `syslog`.

```
# * */2 * * * cd /home/pelangimon/sisop-praktikum-modul-1-2023-bs-d03/soal4 && /home/pelangimon/sisop-praktikum-modul-1-2023-bs-d03/soal4/log_encrypt.sh
```
Crontab diprogram untuk berjalan setiap jam ke-2 dengan file script menggunakan absolute path. Dipanggil "cd" agar crontab menjalankan file pada directory tersimpannya file .sh sehingga hasil enkripsi tersimpan pada directory yang sama.

###### log_decrypt.sh
Cara kerja decryption serupa dengan encryption karena memanfaatkan sifat looping dari caesar cipher.
- 'a' digeser maju 10 menjadi 'k'
- 'k' digeser maju 16 (26-10) menjadi 'a' karena looping kembali ke awal.

**Script harus disertai 1 argumen, yaitu nama file yang akan didekripsi.**
```
abs_path=./$1
```

Membuat absolute path menggunakan nama file yang akan didekripsi.
```
a=$1
curHour=${a:0:2}
curHour=$((10#$curHour))
curHour=$((26-$curHour))
```
Mengambil nilai jam dari nama file lalu mencari key untuk dekripsi.
```
az=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
az_cap=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
while IFS= read -r line; do
	echo $line | tr "${az:0:26}${az_cap:0:26}" "${az:$curHour:26}${az_cap:$curHour:26}" >> "decrypted_$1.txt"
done < "$abs_path"
```
Melakukan proses serupa seperti `log_encrypt.sh` tetapi menerima input file yang akan didekripsi (`$rel_path`).
